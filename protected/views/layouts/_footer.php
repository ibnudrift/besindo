<?php 
$e_activemenu = $this->action->id;
$controllers_ac = $this->id;

$active_menu_pg = $controllers_ac.'/'.$e_activemenu;
?>
<div class="backs_block_topfooter py-5 <?php echo ($active_menu_pg == 'home/index')? 'set_home':'set_inside' ?>">
    <div class="prelatife container">
        <div class="inners py-2 text-center">
            <h3>Get In Touch With Besindo</h3>
            <p>Our customer service team will help you with any information on our products, please feel free to leave your contact information below.</p>
            <div class="py-2"></div>

            <form class="form-inline justify-content-center">
              <input type="text" class="form-control mr-sm-2" id="in_name" placeholder="Your Name">
              <input type="email" class="form-control mr-sm-2" id="in_email" placeholder="Your Email">
              <input type="text" class="form-control mr-sm-2" id="in_phone" placeholder="Your Mobile Phone">
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>

            <div class="clear"></div>
        </div>
    </div>
</div>

<footer class="foot">
    <div class="tops_foot py-5">
        <div class="prelatife container">
            <div class="inners py-0">

                <div class="lists_blc_icons text-center justify-content-center">
                    <div class="row">
                        <div class="col-md-15">
                            <div class="texts">
                                <div class="icons"><img src="<?php echo $this->assetBaseurl.'d-icons_foot_1.jpg' ?>" alt="" class="img-fluid"></div>
                                <div class="info">
                                    <small>Whatsapp Online</small>
                                    <p><a target="_blank" href="https://wa.me/628568973798">0856 8973798 (Click to chat)</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-15">
                            <div class="texts">
                                <div class="icons"><img src="<?php echo $this->assetBaseurl.'d-icons_foot_2.jpg' ?>" alt="" class="img-fluid"></div>
                                <div class="info">
                                    <small>Email</small>
                                    <p><a href="mailto:marketing@besindo.net">marketing@besindo.net</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-15">
                            <div class="texts">
                                <div class="icons"><img src="<?php echo $this->assetBaseurl.'d-icons_foot_3.jpg' ?>" alt="" class="img-fluid"></div>
                                <div class="info">
                                    <small>Instagram</small>
                                    <p><a target="_blank" href="https://instagram.com/besindopackaging">@besindopackaging</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-15">
                            <div class="texts">
                                <div class="icons"><img src="<?php echo $this->assetBaseurl.'d-icons_foot_4.jpg' ?>" alt="" class="img-fluid"></div>
                                <div class="info">
                                    <small>Tokopedia</small>
                                    <p><a target="_blank" href="https://tokopedia.com/bespac">bespac</a></p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>
        </div>
    </div>

    <div class="blocks_white py-4 bg-white">
        <div class="prelatife container">
            <div class="row py-2">
                <div class="col-md-30">
                    <div class="lgo_footers"><a href="#"><img src="<?php echo $this->assetBaseurl ?>lgo-footers.jpg" alt="" class="img img-fluid"></a></div>
                </div>
                <div class="col-md-30">
                    <div class="d-block d-sm-none py-2"></div>
                    <div class="t-copyrights text-right">
                        <p class="m-0">Copyright &copy; 2020 - PT. Beta Sinarindo - manufacturer of polyfoam & air bubble sheet products.</p>
                        
                        All rights reserved. Website design by <a title="Website Design Surabaya" target="_blank" href="https://www.markdesign.net/">Mark Design</a>.
                    </div>
                </div>
            </div>
            <div class="d-block d-sm-none py-3"></div>
            <div class="clear"></div>
        </div>
    </div>
</footer>

<section class="live-chat">
    <div class="row">
        <div class="col-md-60">
            <div class="live">
                <!-- <a href="https://wa.me/6281246666968"> -->
                <a href="https://wa.me/628568973798">
                    <img src="<?php echo $this->assetBaseurl; ?>icon-whatsapp-float.svg" class="img img-fluid" alt="">
                </a>
            </div>
        </div>
    </div>
</section>

<section class="nfoot_mob_wa d-block d-sm-none">
    <div class="row">
        <div class="col-md-60">
            <div class="nx_button">
                <a href="https://wa.me/628568973798"><i class="fa fa-whatsapp"></i> Whatsapp</a>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    .imgs_vold img{
        max-width: 255px;
    }
    .live-chat .live img{
        max-width: 175px;
    }

    @media screen and (max-width: 767px) {
        .caption-slider-home .bxsl_tx_fcs h3, .caption-slider-home .bxsl_tx_fcs h4{
            font-size: 30px;
        }
    }
</style>