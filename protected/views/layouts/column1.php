<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<?php echo $this->renderPartial('//layouts/_header', array()); ?>

<?php 
// get slides
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('active = "1"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->order = 't.urutan ASC';
$slides = Slide::model()->findAll($criteria);
?>
<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide">
    <div class="container cont-fcs">
        <div id="myCarousel_home" class="carousel slide" data-ride="carousel" data-interval="4500">
            <div class="carousel-inner">
                <?php foreach ($slides as $keys => $value): ?>
                <div class="carousel-item <?php if ($keys == 0): ?>active<?php endif ?> home-slider-new">
                    <img class="fcs_dekstop w-100 d-none d-sm-block" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920,746, '/images/slide/'. $value->image, array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="First slide" style="background-repeat: no-repeat; background-size: cover;">
                    <img class="fcs_mob d-block d-sm-none img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1080,2022, '/images/slide/'. $value->image2, array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="">
                    <div class="carousel-caption caption-slider-home d-none hide hidden">
                        <div class="prelative container">
                            <div class="bxsl_tx_fcs text-left">
                                <h4>Besindo Polyfoam & Air Bubble Sheet are everywhere.</h4>
                                <div class="py-2"></div>
                                <a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>" class="btn btn-link btns_bdefaults">Read More</a>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
           <div class="carousel-button-native">
                <div class="prelative container">
                    <ol class="carousel-indicators text-center justify-content-center">
                        <?php foreach ($slides as $keys => $value): ?>
                        <li data-target="#myCarousel_home" data-slide-to="<?php echo $keys ?>" class="<?php echo ($keys == 0)? "active":"" ?>"></li>
                        <?php endforeach ?>
                    </ol>
                </div>
            </div>

            <div class="caption-slider-home">
                <div class="prelative container">
                    <div class="bxsl_tx_fcs text-left">
                        <!-- <h4>Besindo Polyfoam & Air Bubble Sheet is essential in your every day routine...</h4> -->
                        <h4>Besindo is one of the leading manufacturer of Polyfoam and Air Bubble Sheet in Indonesia.</h4>
                        <div class="py-2"></div>
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>" class="btn btn-link btns_bdefaults">Read More</a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

<?php echo $content ?>

<script type="text/javascript">
    // $(document).ready(function(){
        //     if ($(window).width() > 768) {
        //         var $item = $('#myCarousel_home.carousel .carousel-item'); 
        //         var $wHeight = $(window).height();
        //         $item.eq(0).addClass('active');
        //         $item.height($wHeight); 
        //         $item.addClass('full-screen');

        //         $('#myCarousel_home.carousel img.fcs_dekstop').each(function() {
        //           var $src = $(this).attr('src');
        //           var $color = $(this).attr('data-color');
        //           $(this).parent().css({
        //             'background-image' : 'url(' + $src + ')',
        //             'background-color' : $color
        //           });
        //           $(this).remove();
        //         });

        //         $(window).on('resize', function (){
        //           $wHeight = $(window).height();
        //           $item.height($wHeight);
        //         });

        //         $('#myCarousel_home.carousel').carousel({
        //           interval: 4500,
        //           pause: "false"
        //         });
        //     }
        // });
</script>

<?php echo $this->renderPartial('//layouts/_footer', array()); ?>
<?php $this->endContent(); ?>
