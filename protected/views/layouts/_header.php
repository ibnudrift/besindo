<?php 
$e_activemenu = $this->action->id;
$controllers_ac = $this->id;

$active_menu_pg = $controllers_ac.'/'.$e_activemenu;

$criteria = new CDbCriteria;
$criteria->addCondition('active = "1"');
$criteria->order = 't.date_input ASC';
$mod_kategh = ViewGallery::model()->findAll($criteria);
?>

<header class="head headers <?php if ($active_menu_pg == 'home/index' or $active_menu_pg == 'home/abouthistory' or $active_menu_pg == 'home/aboutquality' or $active_menu_pg == 'home/aboutcareer'): ?>homes_head<?php endif ?>">
  <div class="prelative container d-none d-sm-block">
    <div class="">
      <div class="row">
        <div class="col-md-30 col-lg-30">
          <div class="logo_heads">
            <div class="d-inline-block align-middle logos">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
                <img src="<?php echo $this->assetBaseurl; ?>logo-head.jpg" alt="<?php echo Yii::app()->name; ?>" class="img img-fluid">
              </a>
            </div>
            <div class="d-inline-block align-middle mx-3 lines_middlen_greyline"></div>
            <div class="d-inline-block align-middle taglines_head">
              <span>Polyfoam & Air Bubble Sheet</span>
            </div>
          </div>
        </div>

        <div class="col-md-30 col-lg-30">
          
          <div class="float-right">
            <div class="rights_factory_info text-right d-inline-block align-middle mr-1">
              <img src="<?php echo $this->assetBaseurl.'hed_manufacture_top.jpg'; ?>" alt="" class="img img-fluid">
              <div class="clear"></div>
            </div>
            <div class="d-inline-block align-middle taglines_head ml-4">
              <img src="<?php echo $this->assetBaseurl.'logo_sgs.svg'; ?>" alt="SGS ISO System Certification - Standard" class="img img-fluid" style="max-width: 55px;">
            </div>
          </div>
          
          <div class="clear clearfix"></div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="bottoms_blue_head py-2 d-none d-sm-block">
    <div class="prelatife container">
      <div class="row py-1">
        <div class="col-md-45">
          <div class="text-left top-menu">
              <ul class="list-inline mb-0">
                <li class="list-inline-item <?php echo ($active_menu_pg == 'home/index')? 'active' : '' ?>">
                  <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a>
                </li>
                <li class="list-inline-item <?php echo ($active_menu_pg == 'home/about')? 'active' : '' ?>">
                  <a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About Us</a>
                </li>
                <li class="list-inline-item <?php echo ($active_menu_pg == 'home/products')? 'active' : '' ?>">
                  <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Our Products</a>
                </li>
                <li class="list-inline-item <?php echo ($active_menu_pg == 'home/industry')? 'active' : '' ?>">
                  <a href="<?php echo CHtml::normalizeUrl(array('/home/industry')); ?>">Industry Application</a>
                </li>
                <li class="list-inline-item <?php echo ($active_menu_pg == 'home/quality')? 'active' : '' ?>">
                  <a href="<?php echo CHtml::normalizeUrl(array('/home/quality')); ?>">Quality Statement</a>
                </li>
                <li class="list-inline-item <?php echo ($active_menu_pg == 'blog/index')? 'active' : '' ?>">
                  <a href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">Blog</a>
                </li>
                <li class="list-inline-item <?php echo ($active_menu_pg == 'home/contact')? 'active' : '' ?>">
                  <a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact Us</a>
                </li>
              </ul>
            </div>
        </div>
        <div class="col-md-15">
          <div class="text-right wa_nsx_head">
            Whatsapp&nbsp; <a href="https://wa.me/628568973798"><i class="fa fa-whatsapp"></i> &nbsp;0856 8973798</a>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  
  <div class="d-block d-sm-none">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
        <img src="<?php echo $this->assetBaseurl.'logo-head.jpg' ?>" alt="<?php echo Yii::app()->name; ?>" class="img img-fluid">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Our Products</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/industry')); ?>">Industry Application</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/quality')); ?>">Quality Statement</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">Blog</a>
          </li>
          
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact Us</a>
          </li>
        </ul>
      </div>
    </nav>
  </div>

</header>
