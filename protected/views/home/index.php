<section class="backs_home1 back-white py-5">
    <div class="prelatife container">
        <div class="inners py-5 text-center">
            <div class="py-2"></div>
            <div class="tops_title d-block mx-auto mw745">
                <h2>Besindo Polyfoam & Air Bubble Sheet are everyday essentials.</h2>
                <p>We are aware that the protection and safety of your products and items are crucial during transit. Besindo assures to provide the highest quality products and exceptional service through a wide range of Polyfoam and Air Bubble Sheet in different shapes and sizes. Please feel free to browse through our products.</p>
            </div>
            <div class="py-3"></div>
            <?php 
                $arr1 = [
                            [
                                'pict'=>'banners_hmt_1.jpg',
                                'name'=>'Polyfoam Sheet/Roll',
                            ],
                            [
                                'pict'=>'banners_hmt_2.jpg',
                                'name'=>'Air Bubble Sheet/Roll',
                            ],
                            [
                                'pict'=>'banners_hmt_3.jpg',
                                'name'=>'Air Bubble / Polyfoam Pouches',
                            ],
                        ];
            ?>
            <div class="lists_perdana_home1">
                <div class="row">
                    <?php foreach ($arr1 as $key => $value): ?>
                    <div class="col-md-20">
                        <div class="items">
                            <div class="pict"><img src="<?php echo $this->assetBaseurl.$value['pict']; ?>" alt="" class="img img-fluid d-block mx-auto"></div>
                            <div class="info py-2">
                                <h4><?php echo $value['name'] ?></h4>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>

            <div class="clear clearfix"></div>
        </div>
    </div>
</section>

<section class="backs_home2 pt-5">
    <div class="prelatife container">
        <div class="inners">
            <div class="py-4"></div>

            <div class="row">
                <div class="col-md-30">
                    <div class="pictures">
                        <img src="<?php echo $this->assetBaseurl; ?>banner-qlty_homes.png" alt="" class="img-fluid d-block mx-auto">
                    </div>
                </div>
                <div class="col-md-30">
                    <div class="info text-left">
                        <div class="py-5 d-none d-sm-block"></div>
                        <div class="py-3 d-block d-sm-none"></div>
                        <h3>Quality Commitment</h3>
                        <p>We at Besindo are commited to provide the highest quality Polyfoam and Air Bubble Sheet products. For over 30 years, we have put down a zero tolerance policy for failure by performing rigorous quality testing in all aspects of our production. We believe strict quality commitment is quintessential in building lasting business relationships.</p>
                        <div class="py-2 my-1"></div>
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/quality')); ?>" class="btn btn-default btns_bluesn_default">Quality Statement</a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>

            <div class="clear clearfix"></div>
        </div>
    </div>
</section>

<section class="backs_home3 back-fort_variant py-5">
    <div class="prelatife container">
        <div class="inners py-5 text-center">
            <div class="row">
                <div class="col-md-20">
                    <div class="texts">
                        <h3 class="counting">1,750+</h3>
                        <p>Product Variants</p>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="col-md-20">
                    <div class="texts">
                        <h3 class="counting">30+</h3>
                        <p>Years of Experience</p>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="col-md-20">
                    <div class="texts">
                        <h3 class="counting">250+</h3>
                        <p>Industrial Consumers</p>
                        <div class="clear"></div>
                    </div>
                </div>
                <!-- <div class="col-md-15">
                    <div class="texts">
                        <h3 class="counting">80tons</h3>
                        <p>Monthly Capacity</p>
                        <div class="clear"></div>
                    </div>
                </div> -->                
            </div>
            <div class="clear clearfix"></div>
        </div>
    </div>
</section>

<section class="backs_home4 back-human_application py-5 my-auto">
    <div class="prelatife container">
        <div class="inners py-5 text-center">
            <div class="row mx-auto justify-content-center py-4">
                <div class="col-md-46 my-auto">
                    <h3>Industry Application</h3>
                    <div class="py-2"></div>
                    <h5>Besindo's Polyfoam and Air Bubble Sheet products play an important role in everyday life. Whether it's industrial application such as architectural insulation to commercial use protecting valuable goods, Besindo is everywhere!</h5>
                    <p><strong>Besindo Is Everywhere!</strong></p>
                    <p>Besindo partners with major key industries in Indonesia and exports to countries all over Asia and Europe. With a wide range of products, Besindo manufactures and distributes Polyfoam and Air Bubble Sheet.</p>
                    <div class="py-2"></div>
                    <a href="<?php echo CHtml::normalizeUrl(array('/home/industry')); ?>" class="btn btn-info btns_custom">Learn More About Polyfoam & Air Bubble Sheet Industry Application</a>
                </div>
            </div>
            <div class="clear clearfix"></div>
        </div>
    </div>
</section>