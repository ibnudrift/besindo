<section class="hero-inside-pages prelatife">
    <div class="picture_big"><img src="<?php echo $this->assetBaseurl ?>hero-quality.jpg" alt="" class="img img-fluid w-100"></div>
    <div class="caption-insides-top">
        <div class="inners wow fadeInUp">
            <div class="d-block mx-auto maw805">
                <h1>Quality Statement</h1>
                <div class="py-2"></div>
                <p>Besindo inspects and verifies all Polyfoam and Air Bubble Sheet products, reassuring all processes are free from defects and flaws, attaining the highest level of quality.</p>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>


<section class="middle_inside_wrap">

    <section class="bg-white insides_topback1_qualitys py-5 backs_agent">
        <div class="prelatife container">
            <div class="inners pt-5">
                <div class="content-text text-center tops_content_quality d-blcok mx-auto wow fadeInDown">
                    <h2>4 Basic Fundamentals Of Our Quality Process</h2>
                    <div class="clear"></div>
                </div>
            <div class="py-4"></div>

            <?php 
            $lists_industry = [
                                [
                                    'picture'=>'ls_icon_quality_1.jpg',
                                    'title'=>'Professionalism',
                                    'desc'=>'<p>Professionalism is a clear stand-out in all of Besindo personnel. Professionalism is obvious towards customer interaction, but even in an internal zone, we treat fellow employees with respect. We take each other seriously as professionals, and we carry out our work professionally.</p>',
                                ],
                                [
                                    'picture'=>'ls_icon_quality_2.jpg',
                                    'title'=>'Accountability',
                                    'desc'=>'<p>Besindo personnel are always answerable for all actions and results. All of Besindo personnel take acknowledgement at work - knowing that we are all very important and we are part of the process. Even the smallest action will make a difference.</p>',
                                ],
                                [
                                    'picture'=>'ls_icon_quality_3.jpg',
                                    'title'=>'Integrity',
                                    'desc'=>'<p>Besindo strives to be an industry leader and a well-respected partner by maintaining the highest ethical standards and satisfaction.</p>',
                                ],
                                [
                                    'picture'=>'ls_icon_quality_4.jpg',
                                    'title'=>'Collaboration',
                                    'desc'=>'<p>Besindo`s research and development team tirelessly pursues to innovate, develop, and design both products and solutions that best fits each of our clients.</p>',
                                ],                                
                              ];
            ?>

            <div class="lists_quality_fundam_data">
                <div class="row justify-content-center text-center">
                <?php foreach ($lists_industry as $key => $value): ?>
                    <div class="col-md-15">
                        <div class="banner-pict mb-3 pb-2"><img src="<?php echo $this->assetBaseurl.$value['picture'] ?>" alt="" class="img img-fluid"></div>
                        <div class="texts">
                            <h3><?php echo $value['title']; ?></h3>
                            <?php echo $value['desc']; ?>
                        </div>
                    </div>
                <?php endforeach ?>
                </div>
            </div>
            <div class="py-4"></div>
            <div class="py-2"></div>

            <div class="clear"></div>
        </div>
        </div>
    </section>


    <section class="bg-blues blocks_commitments_quality py-5">
        <div class="prelatife container">
            <div class="inners py-5">
                <div class="py-3"></div>
                <div class="content-text text-center d-blcok mx-auto wow fadeInDown">
                    <h4><b>How We Do It</b></h4>
                    <h3>We are committed to deliver the best Polyfoam & Air Bubble Sheet</h3>
                    <p>Strict quality control at Besindo is critical to consistently achieve the highest standards across our Polyfoam & Air Buble Sheet products. Quality Control is applied since the first stage within our processing operations throughout plastic material sourcing and selection, purity screening and testing on the laboratory, product packaging and delivery perfections.</p>
                    <div class="clear"></div>
                </div>
                <div class="py-3"></div>

                <div class="lists_wedo_data">
                    <div class="row text-center">
                        <div class="row">
                            <div class="col-md-12 col-xs-30">
                                <div class="box_items">
                                    <div class="picture mb-2">
                                        <img src="<?php echo $this->assetBaseurl ?>thumbnail-receiving-raw-material-besindo.jpg" alt="" class="img img-fluid w-100">
                                    </div>
                                    <div class="infos">
                                        <p>Receiving Raw Material</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-30">
                                <div class="box_items">
                                    <div class="picture mb-2">
                                        <img src="<?php echo $this->assetBaseurl ?>qualitys-inspec-1.jpg" alt="" class="img img-fluid w-100">
                                    </div>
                                    <div class="infos">
                                        <p>Material Inspection</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-30">
                                <div class="box_items">
                                    <div class="picture mb-2">
                                        <img src="<?php echo $this->assetBaseurl ?>qualitys-inspec-2.jpg" alt="" class="img img-fluid w-100">
                                    </div>
                                    <div class="infos">
                                        <p>Manufacturing Quality Control</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-30">
                                <div class="box_items">
                                    <div class="picture mb-2">
                                        <img src="<?php echo $this->assetBaseurl ?>qualitys-inspec-3.jpg" alt="" class="img img-fluid w-100">
                                    </div>
                                    <div class="infos">
                                        <p>End Result Inspection</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-30">
                                <div class="box_items">
                                    <div class="picture mb-2">
                                        <img src="<?php echo $this->assetBaseurl ?>qualitys-inspec-4.jpg" alt="" class="img img-fluid w-100">
                                    </div>
                                    <div class="infos">
                                        <p>Pre Shipment Inspection</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="py-3"></div>
                <div class="clear"></div>
            </div>
        </div>
    </section>

</section>