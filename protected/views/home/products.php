<section class="hero-inside-pages prelatife">
    <div class="picture_big"><img src="<?php echo $this->assetBaseurl ?>hero-products.jpg" alt="" class="img img-fluid w-100"></div>
    <div class="caption-insides-top">
        <div class="inners wow fadeInUp">
            <h1>Our Products</h1>
            <div class="py-2"></div>
            <p>Strategic product features and customization that focus on <br>productivity, growth, and customer experience.</p>
            <div class="clear"></div>
        </div>
    </div>
</section>


<section class="middle_inside_wrap">

    <section class="bg-white insides_topback1_qualitys py-5 backs_agent">
        <div class="prelatife container">
            <div class="inners pt-5">
                <div class="content-text text-center tops_content_quality d-blcok mx-auto wow fadeInDown">
                    <h2>We believe our products can bring additional value and transform your business to the next level.</h2>
                    <div class="py-1"></div>
                    <p>We are aware that the protection and safety of your products and items are crucial during transit. Besindo assures to provide the highest quality and services through a wide range of Polyfoam and Air Bubble Sheet in various shapes and sizes. Please feel free to browse our products.</p>
                    <div class="clear"></div>
                </div>
            <div class="py-3"></div>

            <?php 
                $arr1 = [
                            [
                                'pict'=>'banners_hmt_1.jpg',
                                'name'=>'Polyfoam Sheet/Roll',
                            ],
                            [
                                'pict'=>'banners_hmt_2.jpg',
                                'name'=>'Air Bubble Sheet/Roll',
                            ],
                            [
                                'pict'=>'banners_hmt_3.jpg',
                                'name'=>'Air Bubble / Polyfoam Pouches',
                            ],
                        ];
            ?>
            <div class="lists_perdana_home1">
                <div class="row">
                    <?php foreach ($arr1 as $key => $value): ?>
                    <div class="col-md-20">
                        <div class="items">
                            <a href="<?php echo CHtml::normalizeUrl(array('/home/product_detail', 'id'=> $key,'name'=> Slug::Create($value['name']) )); ?>">
                                <div class="pict"><img src="<?php echo $this->assetBaseurl.$value['pict']; ?>" alt="" class="img img-fluid d-block mx-auto"></div>
                                <div class="info py-2">
                                    <h4><?php echo $value['name'] ?></h4>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>

            <div class="py-4"></div>
            <div class="py-3"></div>

            <div class="clear"></div>
        </div>
        </div>
    </section>

</section>