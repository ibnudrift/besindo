<section class="hero-inside-pages prelatife">
    <div class="picture_big"><img src="<?php echo $this->assetBaseurl ?>hero-about.jpg" alt="" class="img img-fluid w-100"></div>
    <div class="caption-insides-top">
        <div class="inners wow fadeInUp">
            <div class="d-block mx-auto maw805">
                <h1>About Us</h1>
                <div class="py-2"></div>
                <p>A manufacturer of Polyfoam and Air Bubble Sheet committed to bring you quality products, competitive prices, and reliable partnership.</p>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>

<section class="middle_inside_wrap">

    <section class="bg-white2 insides_topback1 py-5">
        <div class="prelatife container">
            <div class="inners py-4 wow fadeInDown">
                <div class="row justify-content-center">
                    <div class="col-md-55">
                        <div class="content-text text-center tops_content_about">
                            <h2 class="small_title">We are here to help.</h2>
                            <p>Besindo operates as an extension of our client's product quality delivery experience. We partner with our clients to solve their resource challenges by creating new solutions derived from our unrivaled knowledge and expertise from the industry. Our solutions are created intentionally to pursue efficiency and enhanced commerce through fulfillment and packaging solutions to protect the worldwide movement of goods.</p>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </section>

    <section class="back-white insides_topback2 py-5">
        <div class="prelatife container">
            <div class="inners py-5 wow fadeInDown">
                <div class="content-text text-left">

                    <div class="row">
                        <div class="col-md-36">
                            <div class="picture banners"><img src="<?php echo $this->assetBaseurl.'banner-about-1.jpg' ?>" alt="" class="img img-fluid"></div>
                        </div>
                        <div class="col-md-24 my-auto">
                            <h3>Who We Are</h3>
                            <p>Established in 1991, PT. Beta Sinarindo is a leading manufacturer of plastic packaging products. Our products go by the name Besindo and Besindo Packaging (BESPAC) which includes Polyfoam and Air Bubble Sheet that are offered in multiple different sizes.</p>
                            <p>Adopting world-class manufacturing standard at every stage of the process, we promise to deliver the very best quality products while maintaining efficient use of our resources. </p>
                            <p>Here at Besindo, we are commited to provide highest quality customer experience by building trust and having cost competitiveness.</p>
                        </div>
                    </div>

                    <div class="py-4 my-3 d-none d-sm-block"></div>
                    <div class="py-3 d-block d-sm-none"></div>

                    <div class="row">
                        <div class="col-md-24 my-auto order-2 order-sm-1">
                            <h3>Our Commitment</h3>
                            <p>Besindo is committed to demonstrate operational excellence and help solve our customers' most critical packaging challenges.</p>
                        </div>
                        <div class="col-md-36 order-1 order-sm-2">
                            <div class="picture banners"><img src="<?php echo $this->assetBaseurl.'banner-about-2.jpg' ?>" alt="" class="img img-fluid"></div>
                            <!-- <div class="d-block d-sm-none py-3"></div> -->
                        </div>
                    </div>

                    <div class="py-4 my-3 d-none d-sm-block"></div>
                    <div class="py-3 d-block d-sm-none"></div>

                    <div class="row">
                        <div class="col-md-36">
                            <div class="picture banners"><img src="<?php echo $this->assetBaseurl.'banner-about-3.jpg' ?>" alt="" class="img img-fluid"></div>
                            <!-- <div class="d-block d-sm-none py-3"></div> -->
                        </div>
                        <div class="col-md-24 my-auto">
                            <h3>Our Team</h3>
                            <p>Led by industry veterans, Besindo is comprised of trustworthy experts across a range of disciplines including product designers to product specifiers with experience on some of the most complex and interesting cases.</p>
                        </div>
                    </div>

                </div>
                <div class="d-none d-sm-block py-3"></div>
                <div class="clear"></div>
            </div>
        </div>
    </section>

    <section class="back-blues insides_topback4 py-5">
        <div class="prelatife container wow fadeInDown">
            <div class="inners py-5 content-text text-left">
                <div class="row">
                    <div class="col-md-60">
                        <div class="texts">
                        <h4 class="titles_visi">Our Vision</h4>
                        <div class="row">
                            <div class="col-md-5">
                                
                            </div>
                            <div class="col-md-55">
                                <div class="texts">
                                    <p>To become one of the leading plastic packaging companies that competes both on national and international scale</p>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-60">
                        <div class="py-4"></div>
                        <div class="texts">
                        <h4 class="titles_visi">Our Mission</h4>
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-55">
                                <div class="texts">
                                    <ul><li>Committed to providing high quality products.</li><li>Have optimal performance standards to increase productivity effectively and efficiently.</li><li>Maximizing service commitment and maintaining customer trust.</li></ul>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

                <div class="clear clearfix"></div>
            </div>
        </div>
    </section>

    <div class="clear"></div>
</section>