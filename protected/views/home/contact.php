<section class="hero-inside-pages prelatife">
    <div class="picture_big"><img src="<?php echo $this->assetBaseurl ?>hero-contact.jpg" alt="" class="img img-fluid w-100"></div>
    <div class="caption-insides-top">
        <div class="inners wow fadeInUp">
            <div class="d-block mx-auto maw805">
                <h1>Contact Us</h1>
                <div class="py-2"></div>
                <p>We will be at your service to all your needs of information regarding <br>polyfoam & air bubble sheet product specs and other technical matters.</p>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>


<section class="middle_inside_wrap">

    <section class="bg-white insides_topback1_qualitys py-5 backs_agent">
        <div class="prelatife container">
            <div class="inners pt-5">
                <div class="content-text text-center d-blcok mx-auto wow fadeInDown">
                    <div class="row py-4">
                        <div class="col-md-30">
                            <h4>Factory</h4>
                            <p>Lippo Cikarang<br>Bekasi International Industrial Estate<br>Blok C 10/10, Cikarang 17530</p>
                            <p><a href="#"><i class="fa fa-map-marker"></i><br><b>View on Google Map</b></a></p>
                            <p>Telephone: 021 897 3489<br>
                            Fax: 021 897 3490</p>
                        </div>
                        <div class="col-md-30">
                            <h4>Office</h4>
                            <p>The Belleza Permata Hijau <br>Office Tower 27th Floor<br>Jl. Arteri Permata Hijau No. 34, Jakarta 12210</p>
                            <p><a target="_blank" href="https://goo.gl/maps/tYymU8giayA9SBYm8"><i class="fa fa-map-marker"></i><br><b>View on Google Map</b></a></p>
                            <p>Telephone: 021 256 75401 <br>
                            Fax: 021 536 62055</p>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>

            <div class="clear"></div>
        </div>
        </div>
    </section>

</section>