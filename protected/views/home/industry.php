<section class="hero-inside-pages prelatife">
    <div class="picture_big"><img src="<?php echo $this->assetBaseurl ?>hero-industry2.jpg" alt="" class="img img-fluid w-100"></div>
    <div class="caption-insides-top">
        <div class="inners wow fadeInUp">
            <div class="d-block mx-auto maw805">
                <h1>Industry Application</h1>
                <div class="py-2"></div>
                <p>Besindo's Polyfoam and Air Bubble Sheet are everywhere. We are here to support and protect the movement of goods.</p>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>


<section class="middle_inside_wrap">

    <section class="bg-white insides_topback1_qualitys py-5 backs_agent">
        <div class="prelatife container">
            <div class="inners pt-5">
                <div class="content-text text-center tops_content_quality d-blcok mx-auto wow fadeInDown">
                    <h2>Usage & Application of Besindo’s Polyfoam and Air Bubble Sheet.</h2>
                    <div class="clear"></div>
                </div>
            
            <div class="py-5 d-none d-sm-block"></div>
            <div class="py-4 d-block d-sm-none"></div>
            
            <?php 
            $lists_industry = [
                                [
                                    'picture'=>'ban-industry-1.jpg',
                                    'title'=>'Protective Packaging',
                                    'desc'=>'<ul><li>Electronic goods, home appliances e.g. Television, Home Audio, Water Pump, Washing Machine, Music Instruments, etc.</li><li>Anti-static Sheets for Electronic Integrated Circuit Boards, and Precision Equipment</li><li>Ceramicware, glassware, tiles, crystal, etc.</li><li>Office Equipment and Laboratory Instruments</li><li>Furniture and Furnishings</li></ul>',
                                ],
                                [
                                    'picture'=>'ban-industry-2.jpg',
                                    'title'=>'Building & Construction',
                                    'desc'=>'<ul><li>Under-Layer Material for Carpets and Flooring Tiles.</li><li>Heat Insulation for Roofing Purposes. </li><li>Sound Insulation for Walls, Sound Rooms and Other Civil Work.</li><li>Waterproofing in Other Civil and Construction Applications</li></ul>',
                                ],
                                [
                                    'picture'=>'ban-industry-3.jpg',
                                    'title'=>'Consumer Goods & Products',
                                    'desc'=>'<ul><li>Structural padding for backpacks, utility bags, and other sport bags. </li><li>Footwear support sole</li><li>Floor mats for yoga, judo, wrestling, gymnastics and other sports.</li><li>Food packaging</li><li>Padding for Life Jackets and Other Floatation Devices.</li></ul>',
                                ],
                                [
                                    'picture'=>'ban-industry-4.jpg',
                                    'title'=>'Automotive and General Industry',
                                    'desc'=>'<ul><li>Sun-visors, Head rest and Trunk Mats of Automobiles</li><li>Anti-rust Sheets for Automotive Parts </li><li>Flotation Material for Vessels</li><li>Protection Bags for Automotive Equipment</li><li>Cushioning Material for Seats, Bags, and others.</li><li>Sound Insulation</li></ul>',
                                ],
                                
                              ];
            ?>

            <div class="lists_application_data">
                <?php foreach ($lists_industry as $key => $value): ?>
                <div class="lists_item mb-4">
                    <div class="row">
                        <div class="col-md-30">
                            <div class="banner-pict"><img src="<?php echo $this->assetBaseurl.$value['picture'] ?>" alt="" class="img img-fluid"></div>
                        </div>
                        <div class="col-md-30 my-auto">
                            <div class="texts">
                                <h3><?php echo $value['title']; ?></h3>
                                <?php echo $value['desc']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="py-2"></div>
                <?php endforeach ?>
            </div>

            <div class="clear"></div>
        </div>
        </div>
    </section>

</section>