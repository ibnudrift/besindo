<?php 
    $n_id = intval($_GET['id']);
    $data = ViewGallery::model()->find('t.id = :gal_id', array(':gal_id'=>$n_id));

    $dn_image = GalleryImage::model()->findAll('gallery_id = :gal_id', array(':gal_id'=>$n_id));
?>
<section class="dining-sec-1">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60">
                <div class="top-block wow fadeInDown">
                    <div class="logo">
                        <img class="img img-fluid" src="<?php echo $this->assetBaseurl.'../../images/gallery/'. $data->image; ?>" alt="">
                    </div>
                    <div class="title">
                        <div class="py-4"></div>
                        <div class="nxg_logo d-block mx-auto" style="max-width: 125px;">
                            <img src="<?php echo $this->assetBaseurl ?>logo-nippo.svg" alt="" class="img img-fluid mx-auto">
                        </div>
                        <div class="py-2"></div>
                        <p><?php echo strtoupper($data->title) ?></p>
                    </div>
                    <div class="text">
                        <?php echo $data->sub_title ?>
                    </div>
                    <div class="clear clearfix"></div>
                </div>

                <div class="hr-ver"></div>

                <?php foreach ($dn_image as $key => $value): ?>
                    <div class="image wow fadeIn">
                        <img class="img img-fluid" src="<?php echo $this->assetBaseurl.'../../images/gallery/'. $value->image; ?>" alt="">
                    </div>
                <?php endforeach ?>

                <div class="text-bawah mb-5 pb-5 pt-5 text-center wow fadeInDown">
                    <div class="row">
                        <div class="col-md-10"></div>
                        <div class="col-md-40">
                            <?php echo $data->content ?>                    
                        </div>
                        <div class="col-md-10"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<style type="text/css">
    section.dining-sec-1 .prelative.container .top-block .logo{
        padding-top: 0;
    }
</style>