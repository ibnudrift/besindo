
<section class="middle_inside_wrap outers_back_product_details_pg">

    <section class="insides_topback1_qualitys py-5 backs_transparent">
        <div class="prelatife container">
            <div class="inners pt-3">
                <div class="tops_prd_details_cont text-center">
                    <div class="row">
                        <div class="col-md-20">
                            <div class="out_breadcrumbs">
                                <nav aria-label="breadcrumb">
                                  <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                                    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Our Products</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"><?php echo $names ?></li>
                                  </ol>
                                </nav>
                            </div>
                        </div>
                        <div class="col-md-20">
                                <h3 class="titles m-0"><?php echo $names ?></h3>
                        </div>
                        <div class="col-md-20">
                            <a href="#" class="btn btn-link backs_prd text-right d-block p-0">Back To Products</a>
                        </div>
                    </div>
                </div>

                <div class="py-3"></div> 

                <div class="content-text text-center boxeds_detail_prd d-blcok mx-auto wow fadeInDown">
                    <div class="pictures">
                        <?php if ($_GET['id'] == '0'): ?>
                            <img src="<?php echo $this->assetBaseurl. 'pics_ex_products.jpg'; ?>" alt="" class="img img-fluid">
                        <?php elseif($_GET['id'] == '1'): ?>
                            <img src="<?php echo $this->assetBaseurl. 'besindo-air-bubble.jpg'; ?>" alt="" class="img img-fluid">
                        <?php elseif($_GET['id'] == '2'): ?>
                            <img src="<?php echo $this->assetBaseurl. 'air-polyfoam-bg.jpg'; ?>" alt="" class="img img-fluid">
                        <?php endif ?>
                    </div>
                    <div class="py-3 my-1"></div>

                    <?php if ($_GET['id'] == '2'): ?>
                        <h2>What is Besindo's Air Bubble/Polyfoam Pouch?</h2>
                    <?php else: ?>
                        <h2>What is Besindo’s <?php echo $names ?>?</h2>
                    <?php endif ?>

                    <?php if ($_GET['id'] == '0'): ?>
                        <p>Besindo's Polyfoam Sheet Roll or also known as PE Foam is a lightweight, durable, resilient, closed-cell material. It is most often used as packaging material and agricultural goods due to its outstanding vibration dampening, insulation properties, and high resistance to moisture & chemicals.</p>
                    <?php elseif($_GET['id'] == '1'): ?>
                        <p>Besindo's Air Bubble Sheet Roll or also known as Bubble Wrap is a sheet of plastic with bubbles of air evenly spaced across the surface. It is most commonly used as packaging materials. Having protruding air-filled bubbles, it provides perfect cushioning for fragile items. Keeping all the items safe from knocks, bumps, and shocks, so that they get to their destinations intact and undamaged.</p>
                    <?php elseif($_GET['id'] == '2'): ?>
                        <p>Besindo's Air Bubble/Polyfoam Pouch is a ready to fit product manufactured in accordance to your specified size requirement. We offer tailor made sizes that fits your custom requirements that will keep your products safe from knocks, bumps, and shocks, so that they get to their destinations intact and undamaged.</p>
                    <?php endif ?>

                    <?php if ($_GET['id'] == '2'): ?>
                        <p><strong>Characteristics of Besindo's Air Bubble/Polyfoam Pouch</strong></p>
                    <?php else: ?>
                        <p><strong>Characteristics of Besindo’s <?php echo $names ?></strong></p>
                    <?php endif ?>
                    <div class="py-2"></div>

                    <div class="blocks_detailn_tab">
                        <?php if ($_GET['id'] == '0'): ?>
                        <div class="row">
                            <div class="col-md-12 my-auto text-center">
                                <div class="texts">
                                    <p>Closed cell Structure</p>
                                </div>
                            </div>
                            <div class="col-md-12 my-auto text-center">
                                <div class="texts">
                                    <p>Superior cushioning effect, strength & shock absorbent</p>
                                </div>
                            </div>
                            <div class="col-md-12 my-auto text-center">
                                <div class="texts">
                                    <p>Outstanding chemical resistance & weatherproof properties</p>
                                </div>
                            </div>
                            <div class="col-md-12 my-auto text-center">
                                <div class="texts">
                                    <p>Lightweight, flexible, tear resistance</p>
                                </div>
                            </div>
                            <div class="col-md-12 my-auto text-center">
                                <div class="texts">
                                    <p>Excellent thermal insulation properties</p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="d-none d-sm-block">
                            <div class="py-1 my-1"></div>
                            <div class="lines-grey"></div>
                            <div class="py-1 my-1"></div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 my-auto text-center">
                                <div class="texts">
                                    <p>Non-dusting and hygienic properties</p>
                                </div>
                            </div>
                            <div class="col-md-12 my-auto text-center">
                                <div class="texts">
                                    <p>Odorless</p>
                                </div>
                            </div>
                            <div class="col-md-12 my-auto text-center">
                                <div class="texts">
                                    <p>Anti-static option is available</p>
                                </div>
                            </div>
                            <div class="col-md-12 my-auto text-center">
                                <div class="texts">
                                    <p>Very cost-effective</p>
                                </div>
                            </div>
                            <div class="col-md-12 my-auto text-center">
                                <div class="texts">
                                    <p>Ozone friendly and CFC free</p>
                                </div>
                            </div>
                        </div>
                        <?php elseif($_GET['id'] == '1'): ?>
                        <div class="row">
                            <div class="col-md-12 my-auto text-center">
                                <div class="texts">
                                    <p>Lightweight & easy-to-use</p>
                                </div>
                            </div>
                            <div class="col-md-12 my-auto text-center">
                                <div class="texts">
                                    <p>Durably elastic & strong</p>
                                </div>
                            </div>
                            <div class="col-md-12 my-auto text-center">
                                <div class="texts">
                                    <p>Moisture & water resistant</p>
                                </div>
                            </div>
                            <div class="col-md-12 my-auto text-center">
                                <div class="texts">
                                    <p>Ease of use and handling with day-to-day hand tools</p>
                                </div>
                            </div>
                            <div class="col-md-12 my-auto text-center">
                                <div class="texts">
                                    <p>Good insulation properties</p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="d-none d-sm-block">
                            <div class="py-1 my-1"></div>
                            <div class="lines-grey"></div>
                            <div class="py-1 my-1"></div>
                        </div>

                        <div class="row">
                            <div class="col-md-15 my-auto text-center">
                                <div class="texts">
                                    <p>Odorless</p>
                                </div>
                            </div>
                            <div class="col-md-15 my-auto text-center">
                                <div class="texts">
                                    <p>Anti-static option is available</p>
                                </div>
                            </div>
                            <div class="col-md-15 my-auto text-center">
                                <div class="texts">
                                    <p>Very cost-effective</p>
                                </div>
                            </div>
                            <div class="col-md-15 my-auto text-center">
                                <div class="texts">
                                    <p>Reusable and recyclable</p>
                                </div>
                            </div>
                        </div>

                        <?php elseif($_GET['id'] == '2'): ?>
                            <div class="row">
                                <div class="col-md-12 my-auto text-center">
                                    <div class="texts">
                                        <p>Lightweight & easy-to-use</p>
                                    </div>
                                </div>
                                <div class="col-md-12 my-auto text-center">
                                    <div class="texts">
                                        <p>Durably elastic & strong</p>
                                    </div>
                                </div>
                                <div class="col-md-12 my-auto text-center">
                                    <div class="texts">
                                        <p>Moisture & water resistant</p>
                                    </div>
                                </div>
                                <div class="col-md-12 my-auto text-center">
                                    <div class="texts">
                                        <p>Ease of use and handling with day-to-day hand tools</p>
                                    </div>
                                </div>
                                <div class="col-md-12 my-auto text-center">
                                    <div class="texts">
                                        <p>Good insulation properties</p>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="d-none d-sm-block">
                                <div class="py-1 my-1"></div>
                                <div class="lines-grey"></div>
                                <div class="py-1 my-1"></div>
                            </div>

                            <div class="row">
                                <div class="col-md-15 my-auto text-center">
                                    <div class="texts">
                                        <p>Odorless</p>
                                    </div>
                                </div>
                                <div class="col-md-15 my-auto text-center">
                                    <div class="texts">
                                        <p>Anti-static option is available</p>
                                    </div>
                                </div>
                                <div class="col-md-15 my-auto text-center">
                                    <div class="texts">
                                        <p>Very cost-effective</p>
                                    </div>
                                </div>
                                <div class="col-md-15 my-auto text-center">
                                    <div class="texts">
                                        <p>Reusable and recyclable</p>
                                    </div>
                                </div>
                            </div>
                        <?php endif ?>
                        <div class="clear"></div>
                    </div>
                    <!-- End block details tab -->
                    <div class="py-4"></div>
                    <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>" class="btn btn-light bakcs_t_prd">Back To Products</a>

                    <div class="py-3"></div>

                    <div class="clear"></div>
                </div>
            <div class="clear"></div>
        </div>
        </div>
    </section>

</section>