<section class="hero-inside-pages prelatife">
    <div class="picture_big"><img src="<?php echo $this->assetBaseurl ?>hero-industry2.jpg" alt="" class="img img-fluid w-100"></div>
    <div class="caption-insides-top">
        <div class="inners wow fadeInUp">
            <div class="d-block mx-auto maw805">
                <h1>Blog</h1>
                <!-- <div class="py-2"></div>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea mollitia neque, quod ab amet ex libero, repellat quis sequi, laborum totam cupiditate aspernatur. Eveniet vitae a eius aut alias explicabo..</p> -->
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>


<section class="middle_inside_wrap">

    <section class="bg-white insides_topback1_qualitys py-5 backs_agent">
        <div class="prelatife container">
            <div class="inners pt-5">

           <!-- start list blog -->
            <?php if ($dataBlog): ?>
            <div class="row default-data list_blog_data">
                <?php foreach ($dataBlog->getData() as $key => $value): ?>
                    <div class="col-md-20">
                        <div class="box-content">
                            <div class="image">
                                <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id' => $value->id, 'names' => Slug::Create($value->description->title) )); ?>">
                                    <img class="img img-fluid w-100"src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(620,409, '/images/blog/'. $value->image, array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="">

                                </a>
                            </div>
                            <div class="title pt-3">
                                <h5 class="fs-16"><?php echo ucwords(substr($value->description->title, 0, 45)).'...'; ?></h5>
                            </div>
                            <div class="klik py-1">
                                <a class="fs-12" href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id' => $value->id, 'names' => Slug::Create($value->description->title) )); ?>"><p>Read More</p></a>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
            <?php endif ?>
            <div class="py-4"></div>
            <div class="textaboveheader-landing page">
                  <?php 
                   $this->widget('CLinkPager', array(
                      'pages' => $dataBlog->getPagination(),
                      'header'=>'',
                      'footer'=>'',
                      'lastPageCssClass' => 'd-none',
                      'firstPageCssClass' => 'd-none',
                      'nextPageCssClass' => 'd-none',
                      'previousPageCssClass' => 'd-none',
                      'itemCount'=> $dataBlog->totalItemCount,
                      'htmlOptions'=>array('class'=>'pagination'),
                      'selectedPageCssClass'=>'active',
                  ));
               ?>
              </div>
           <!-- end list blog -->

            <div class="clear"></div>
        </div>
        </div>
    </section>

</section>
