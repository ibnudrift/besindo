<section class="hero-inside-pages prelatife">
    <div class="picture_big"><img src="<?php echo $this->assetBaseurl ?>hero-industry2.jpg" alt="" class="img img-fluid w-100"></div>
    <div class="caption-insides-top">
        <div class="inners wow fadeInUp">
            <div class="d-block mx-auto maw805">
                <h1>Blog</h1>
                <!-- <div class="py-2"></div>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea mollitia neque, quod ab amet ex libero, repellat quis sequi, laborum totam cupiditate aspernatur. Eveniet vitae a eius aut alias explicabo..</p> -->
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>


<section class="middle_inside_wrap">

    <section class="bg-white insides_topback1_qualitys py-5 backs_agent">
        <div class="prelatife container">
            <div class="inners pt-5">

           <!-- start list blog -->
           <div class="row">
            <div class="col-md-15">
            <div class="box-konten-kiri">
              <h5>Blog</h5>
            </div>
            </div>
            <div class="col-md-45 rights_cont_def">
              <h3><?php echo $dataBlog->description->title; ?></h3>
              <div class="clear clearfix"></div>
              <span class="dates"><small><i class="fa fa-calendar"></i> <?php echo date("d F Y", strtotime($dataBlog->date_input)) ?></small></span>
              <div class="clear clearfix"></div>
              <div class="py-2"></div>
              <div class="row feature-data mb-0 mt-0">
                <div class="col-md-60">
                  
                  <div class="featured_car_detail pb-3">
                      <img class="img img-fluid w-100" src="<?php echo $this->assetBaseurl.'../../images/blog/'; ?><?php echo $dataBlog->image ?>" alt="<?php echo $dataBlog->description->title ?>">
                  </div>
                  <div class="contents_det_blog">
                    <?php echo $dataBlog->description->content; ?>

                    <?php if ($dataBlog->link): ?>
                      <p>
                      source: <?php echo $dataBlog->link ?>
                      </p>
                    <?php endif ?>

                    <div class="py-3"></div>
                    <p><a class="fs-15" href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>"><i class="fa fa-chevron-left"></i> Back</a></p>
                    <div class="clear"></div>
                  </div>

                </div>
              </div>

            </div>
          </div>
           <!-- end list blog -->

            <div class="clear"></div>
        </div>
        </div>
    </section>
</section>
<div class="py-3"></div>

<style type="text/css">
    section.product-sec-1 .box-konten-kiri{
        min-height: 600px;
    }
    section.product-sec-1 h3{
      margin-bottom: 15px;
    }
    small, .small{
      font-size: 90%;
    }
</style>


