<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl2 ?>hero-products.jpg');">
    <div class="prelative container py-5">
        <div class="py-5"></div>
        <div class="row py-5">
            <div class="col-md-30 py-5">
                <div class="insides_intext">
                    <h3 class="mb-2">PRODUK</h3>
                    <div class="py-1"></div>
                    <p>Ketahui lebih lanjut tentang varian produk pupuk hasil produksi dan pupuk distribusi oleh EKLIN Fertilizer Group.</p>
                    <div class="clear"></div>
                </div>

            </div>
            <div class="col-md-30"></div>
        </div>
    </div>
</section>

<section class="product-sec-1 py-5 back-white">
    <div class="prelative container py-5">
        <div class="inners content-text text-center">
            
            <div class="blocks_top_category product_pg text-center">
                <h3>Kategori Produk Pupuk</h3>
                <div class="py-3"></div>
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="#">PUPUK NPK</a></li>
                    <li class="list-inline-item"><a href="#">PUPUK UREA</a></li>
                    <li class="list-inline-item"><a href="#">PUPUK PHOSPHAT</a></li>
                    <li class="list-inline-item"><a href="#">PUPUK PUPUK DOLOMITE</a></li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="py-4"></div>
            <div class="lines-grey"></div>
            <div class="py-4 my-2"></div>

            <div class="blocks_listproduct_categorys product_pg">
                <?php for ($k=0; $k < 5; $k++) { ?>
                <div class="blocks_list">
                    <div class="titles">Pupuk NPK</div>
                    <div class="py-3"></div>
                    <div class="row lists_data_categorys">
                        <?php for ($i=1; $i < 5; $i++) { ?>
                        <div class="col-md-15 col-30">
                            <div class="items">
                                <div class="picture">
                                    <a href="<?php echo CHtml::normalizeUrl(array('/product/d_detail')); ?>"><img src="<?php echo $this->assetBaseurl2.'sample-product.jpg' ?>" alt="" class="img img-fluid"></a>
                                    </div>
                                <div class="info">
                                    <h3><a href="<?php echo CHtml::normalizeUrl(array('/product/d_detail')); ?>">Pupuk NPK tipe 1</a></h3>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>

                <?php if ($k != 4): ?>
                    <div class="py-3"></div>
                    <div class="lines-grey"></div>
                    <div class="py-3"></div>
                <?php endif ?>
                <?php } ?>

                <div class="clear"></div>
            </div>            
            <div class="py-2"></div>

            <div class="clear"></div>
        </div>
    </div>
</section>
